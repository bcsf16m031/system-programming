#include <stdio.h>
#include <stdlib.h>

int main(int argc , char* argv[])
{
    double num = strtod(argv[1], NULL);
    
    if (num == (int)num) {
        printf("Error");
        return 0;
    }
    printf("%f", num);
    return 0;
}